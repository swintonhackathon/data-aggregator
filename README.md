# README #

### Running the server

    vagrant ssh
    cd /vagrant
    node server.js
    
### Run the server and exit the vagrant shell use screen


    sudo apt-get install -y screen
    screen
    ... same as above
    CTRL+A CTRL+D

    exit

### Connecting to Mongo from host into guest

edit /etc/mongod.conf and comment out the local interface

    # Listen to local interface only. Comment out to listen on all interfaces.
    #bind_ip = 127.0.0.1


